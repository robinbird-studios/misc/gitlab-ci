module Fastlane
  module Actions
    module SharedValues
      UPLOAD_GOOGLE_PLAY_IAP_CUSTOM_VALUE = :UPLOAD_GOOGLE_PLAY_IAP_CUSTOM_VALUE
    end

    class UploadGooglePlayIapAction < Action
      def self.run(params)
        # fastlane will take care of reading in the parameter and fetching the environment variable:
        UI.message "Parameter API Token: #{params[:json_key]}"

        # sh "shellcommand ./path"

        begin
          client = Supply::Client.make_from_config(params: params)
          FastlaneCore::UI.success("Successfully established connection to Google Play Store.")
          FastlaneCore::UI.verbose("client: " + client.inspect)
        rescue => e
          UI.error("Could not establish a connection to Google Play Store with this json key file.")
          UI.error("#{e.message}\n#{e.backtrace.join("\n")}") if FastlaneCore::Globals.verbose?
        end

        products = client.list_inappproducts(params[:package_name])

        UI.message("Got products: #{products}")
        products.each do |product|
          UI.message("Product: #{product.sku} Price: #{product.default_price.price_micros}")
        end

        UI.message("Inserting product")
        client.insert_inappproduct(params[:package_name], "premium_1", false, "EUR", "0990000", "en-US")

        UI.message("Yeah")

        # Actions.lane_context[SharedValues::UPLOAD_GOOGLE_PLAY_IAP_CUSTOM_VALUE] = "my_val"
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "A short description with <= 80 characters of what this action does"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
        # Define all options your action supports.

        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :json_key,
                                     env_name: "SUPPLY_JSON_KEY",
                                     short_option: "-j",
                                     conflicting_options: [:issuer, :key, :json_key_data],
                                     optional: true, # this shouldn't be optional but is until --key and --issuer are completely removed
                                     description: "The path to a file containing service account JSON, used to authenticate with Google",
                                     code_gen_sensitive: true,
                                     default_value: CredentialsManager::AppfileConfig.try_fetch_value(:json_key_file),
                                     default_value_dynamic: true,
                                     verify_block: proc do |value|
                                       UI.user_error!("Could not find service account json file at path '#{File.expand_path(value)}'") unless File.exist?(File.expand_path(value))
                                       UI.user_error!("'#{value}' doesn't seem to be a JSON file") unless FastlaneCore::Helper.json_file?(File.expand_path(value))
                                     end),
          FastlaneCore::ConfigItem.new(key: :package_name,
                                       env_name: "SUPPLY_PACKAGE_NAME",
                                       short_option: "-p",
                                       description: "The package name of the application to use",
                                       code_gen_sensitive: true,
                                       default_value: CredentialsManager::AppfileConfig.try_fetch_value(:package_name),
                                       default_value_dynamic: true),
          FastlaneCore::ConfigItem.new(key: :timeout,
                                      env_name: "SUPPLY_TIMEOUT",
                                      optional: true,
                                      description: "Timeout for read, open, and send (in seconds)",
                                      type: Integer,
                                      default_value: 300),
          FastlaneCore::ConfigItem.new(key: :root_url,
                                     env_name: "SUPPLY_ROOT_URL",
                                     description: "Root URL for the Google Play API. The provided URL will be used for API calls in place of https://www.googleapis.com/",
                                     optional: true,
                                     verify_block: proc do |value|
                                       UI.user_error!("Could not parse URL '#{value}'") unless value =~ URI.regexp
                                     end),
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['UPLOAD_GOOGLE_PLAY_IAP_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["Your GitHub/Twitter Name"]
      end

      def self.is_supported?(platform)
        # you can do things like
        #
        #  true
        #
        #  platform == :ios
        #
        #  [:ios, :mac].include?(platform)
        #

        platform == :ios
      end
    end
  end
end
