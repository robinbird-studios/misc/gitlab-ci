module Fastlane
  module Actions
    class MsbuildrAction < Action
      def self.run(params)
        fl_platform = Actions.lane_context[Actions::SharedValues::PLATFORM_NAME]
        configuration = params[:configuration]
        platform = params[:platform]
        solution = params[:solution]

        msbuild = params[:msbuild_path] ? File.join(params[:msbuild_path], "msbuild") : "msbuild"

        unless params[:build_ipa].nil?
          if params[:build_ipa] == true
            build_ipa = "true"
          else
            build_ipa = "false"
          end
        end

        command = "#{msbuild} \"#{solution}\""
        params[:targets].each do |target|
          command << " /t:\"#{target}\""
        end
        command << " /p:Configuration=\"#{configuration}\""
        command << " /p:Platform=\"#{platform}\"" if platform

        if fl_platform == :ios
          command << " /p:BuildIpa=#{build_ipa}" if build_ipa
          unless params[:version_code].nil? || params[:plist_path].nil?
            plist_command = "/usr/libexec/PlistBuddy -x -c \"Set :CFBundleVersion #{params[:version_code]}\" #{params[:plist_path]}"
            FastlaneCore::CommandExecutor.execute(command: plist_command, print_all: true, print_command: true)
          end
          unless params[:version_name].nil? || params[:plist_path].nil?
            plist_command = "/usr/libexec/PlistBuddy -x -c \"Set :CFBundleShortVersionString #{params[:version_name]}\" #{params[:plist_path]}"
            FastlaneCore::CommandExecutor.execute(command: plist_command, print_all: true, print_command: true)
          end
          unless params[:upload_crashlytics_debug_symbols].nil?
            command << " /p:FirebaseCrashlyticsUploadSymbolsEnabled=#{params[:upload_crashlytics_debug_symbols]}"
          end
        end

        if fl_platform == :android
          command << " /p:AndroidSdkDirectory=\"#{params[:android_home]}\"" if params[:android_home]
          unless params[:version_code].nil?
            command << " /p:AndroidVersionCodePattern={civersioncode}"
            command << " /p:AndroidVersionCodeProperties=civersioncode=#{params[:version_code]}"

            unless params[:android_string_resources_path].nil?
              crashlytics_replace_command = "if [[ -f \"#{params[:android_string_resources_path]}\" ]]; then sed -i '' 's/\\<string name=\"com.crashlytics.android.build_id\"\\>1\\<\\/string\\>/\\<string name=\"com.crashlytics.android.build_id\"\\>#{params[:version_code]}\\<\\/string\\>/' #{params[:android_string_resources_path]}; fi"
              FastlaneCore::CommandExecutor.execute(command: crashlytics_replace_command, print_all: true, print_command: true)
            end
          end
          unless params[:version_name].nil? || params[:android_manifest_path].nil?
            replace_command = "sed -i '' 's/android:versionName=\"1.0\"/android:versionName=\"#{params[:version_name]}\"/' #{params[:android_manifest_path]}"
            FastlaneCore::CommandExecutor.execute(command: replace_command, print_all: true, print_command: true)
          end

          unless params[:android_sign_with_key_store].nil?
            if params[:android_sign_with_key_store] == true
              UI.message("Android signing info key: #{params[:android_signing_key_store]} with alias #{params[:android_signing_key_alias]}")
            end
          end
          unless params[:android_signing_key_store].nil? || params[:android_signing_key_alias].nil?
            # Convert hexadecimal key back to binary
            key_restore_command = "tr -cd 0-9a-fA-F < #{params[:android_signing_key_store]} | xxd -r -p > #{params[:android_signing_key_store]}.binary"
            FastlaneCore::CommandExecutor.execute(command: key_restore_command, print_all: true, print_command: true)
            command << " /p:AndroidKeyStore=#{params[:android_sign_with_key_store]}"
            command << " /p:AndroidSigningKeyStore=#{params[:android_signing_key_store]}.binary"
            command << " /p:AndroidSigningStorePass=env:#{params[:android_signing_store_password_env]}"
            command << " /p:AndroidSigningKeyAlias=#{params[:android_signing_key_alias]}"
            command << " /p:AndroidSigningKeyPass=env:#{params[:android_signing_key_password_env]}"
          end
        end

        params[:additional_arguments].each do |param|
          command << " #{param}"
        end

        FastlaneCore::CommandExecutor.execute(command: command, print_all: true, print_command: true)
      end

      def self.description
        "Build a Xamarin.iOS or Xamarin.Android project using msbuild"
      end

      def self.authors
        ["fuzzybinary"]
      end

      def self.details
        "Build a Xamarin.iOS or Xamarin.Android project using msbuild"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(
            key: :solution,
              env_name: 'FL_MSBUILD_SOLUTION',
              description: 'path to .sln file',
              verify_block: proc do |value|
                UI.user_error!('File not found'.red) unless File.file? value
              end
          ),

          FastlaneCore::ConfigItem.new(
            key: :targets,
            env_name: 'FL_MSBUILD_TARGET',
            description: 'Targets to build',
            type: Array,
            verify_block: proc do |value|
              UI.user_error!('Must supply one target to msbuild'.red) unless value.length > 0
            end
          ),

          FastlaneCore::ConfigItem.new(
            key: :platform,
            optional: true,
            env_name: 'FL_MSBUILD_PLATFORM',
            description: 'build platform (usually iPhone, iPhoneSimulator, or Android)',
            type: String
          ),

          FastlaneCore::ConfigItem.new(
            key: :configuration,
            env_name: 'FL_MSBUILD_CONFIGURATION',
            description: 'Configuration build type',
            type: String
          ),

          FastlaneCore::ConfigItem.new(
            key: :version_code,
            optional: true,
            env_name: 'FL_MSBUILD_VERSION_CODE',
            description: "Android versionCode and iOS CFBundleVersion. Should be an integer that increases each upload",
            type: Integer,
          ),

          FastlaneCore::ConfigItem.new(
            key: :version_name,
            optional: true,
            env_name: 'FL_MSBUILD_VERSION_NAME',
            description: "Android versionName and iOS CFBundleShortVersionString. A string that describes your version. ( e.g. 1.3 )",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :plist_path,
            optional: true,
            env_name: 'FL_MSBUILD_PLIST_PATH',
            description: "Path to the Info.plist file for the iOS Xamarin project",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :upload_crashlytics_debug_symbols,
            env_name: 'FL_MSBUILD_UPLOAD_CRASHLYTICS_DEBUG_SYMBOLS',
            description: "Upload debug symbols to Crashlytics to decifer native crashes",
            optional: true,
            is_string: false,
            verify_block: proc do |value|
              UI.user_error!("Invalid value #{value}. It must either be true or false") unless [true, false].include?(value)
            end
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_manifest_path,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_MANIFEST_PATH',
            description: "Path to the AndroidManifest.xml file for the Android Xamarin project",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_string_resources_path,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_STRING_RESOURCES_PATH',
            description: "Path to the Strings.xml file which contains the main string resources. Used to set com.crashlytics.android.build_id",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_sign_with_key_store,
            env_name: 'FL_MSBUILD_ANDROID_SIGN_WITH_KEY_STORE',
            description: "Decide if app should be signed with keystore",
            optional: true,
            is_string: false,
            verify_block: proc do |value|
              UI.user_error!("Invalid value #{value}. It must either be true or false") unless [true, false].include?(value)
            end
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_signing_key_store,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_SIGNING_KEY_STORE',
            description: "AndroidSigningKeyStore",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_signing_store_password_env,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_SIGNING_STORE_PASSWORD_ENV',
            description: "AndroidSigningStorePass Environment Variable Name",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_signing_key_alias,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_SIGNING_KEY_ALIAS',
            description: "AndroidSigningKeyAlias",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_signing_key_password_env,
            optional: true,
            env_name: 'FL_MSBUILD_ANDROID_SIGNING_KEY_PASSWORD_ENV',
            description: "AndroidSigningKeyPass Environment Variable Name",
            type: String,
          ),

          FastlaneCore::ConfigItem.new(
            key: :additional_arguments,
            optional: true,
            env_name: 'FL_MSBUILD_ADDITIONAL_ARGS',
            description: "An array of Additional arguments to msbuild",
            type: Array,
            default_value: []
          ),

          FastlaneCore::ConfigItem.new(
            key: :android_home,
            optional: true,
            env_name: 'ANDROID_HOME',
            description: 'Location of the Anrdoid SDK (defaults to $ANDROID_HOME)',
            type: String
          ),

          FastlaneCore::ConfigItem.new(
            key: :msbuild_path,
            env_name: 'MSBUILD_PATH',
            description: "Location of msbuild",
            optional: true,
            type: String,
            default_value: nil
          ),

          FastlaneCore::ConfigItem.new(
            key: :build_ipa,
            env_name: 'FL_MSBUILD_BUILD_IPA',
            description: "Should build ipa in iOS build",
            optional: true,
            is_string: false,
            verify_block: proc do |value|
              UI.user_error!("Invalid value #{value}. It must either be true or false") unless [true, false].include?(value)
            end
          )
        ]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
