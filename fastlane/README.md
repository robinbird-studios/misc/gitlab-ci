fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## Android
### android unity_package
```
fastlane android unity_package
```
Lane for Android Build Actions
### android xamarin_build
```
fastlane android xamarin_build
```

### android upload_firebase_app_distribution
```
fastlane android upload_firebase_app_distribution
```

### android upload_google_play
```
fastlane android upload_google_play
```


----

## iOS
### ios xamarin_create_icon
```
fastlane ios xamarin_create_icon
```
Lane for iOS Build Actions
### ios xamarin_build
```
fastlane ios xamarin_build
```

### ios unity_package
```
fastlane ios unity_package
```

### ios upload_firebase_app_distribution
```
fastlane ios upload_firebase_app_distribution
```

### ios upload_app_store_connect
```
fastlane ios upload_app_store_connect
```


----

## Mac
### mac unity_package
```
fastlane mac unity_package
```

### mac steam_package
```
fastlane mac steam_package
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
