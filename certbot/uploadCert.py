#!/usr/local/bin/python3

import sys
import os
import requests
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("domain", help="Specify the domain which should be set")
parser.add_argument("projectId", help="The id of the gitlab probject to update")
parser.add_argument("publicKeyFile")
parser.add_argument("privateKeyFile")



args = parser.parse_args()


with open(args.publicKeyFile, "r") as file:
	publicKey = file.read()

with open(args.privateKeyFile, "r") as file:
	privateKey = file.read()


url = "https://gitlab.com/api/v4/projects/" + args.projectId + "/pages/domains/" + args.domain
headers = {"PRIVATE-TOKEN": os.environ["GITLAB_API_TOKEN"]}

payload = { "certificate": publicKey, "key": privateKey }

print("Uploading keys")

r = requests.put(url, headers=headers, data=payload)

print(r.text.strip())
