#!/usr/bin/env bash

SCRIPT_RUNTIME_VERSION=$1

if [ -z "${SCRIPT_RUNTIME_VERSION}" ]; then
    SCRIPT_RUNTIME_VERSION=1
fi

# Checkout Package Manager
BASE_URL=`echo $CI_REPOSITORY_URL | sed "s;\/*$CI_PROJECT_PATH.*;;"`
REPO_URL="${BASE_URL}/robinryf/buu-package-manager.git"
UNITY_PROJECT_PATH="$(pwd)/UnityProject"
UNITY_ASSETS_DIR="${UNITY_PROJECT_PATH}/Assets"
UNITY_PROJECT_SETTINGS_PATH="${UNITY_PROJECT_PATH}/ProjectSettings"
ROOT_PACKAGE_DIR="$(pwd)/rootPackage"

git clone ${REPO_URL} BuuPackageManager
echo "Create Unity project at path ${UNITY_PROJECT_PATH}"
echo "RootPackage: ${ROOT_PACKAGE_DIR}"
${SHARED_CI}/unity3d/execute-unity.sh "${UNITY_PROJECT_PATH}" "-quit -batchmode -scripting-runtime-version latest -noUpm -createProject ${UNITY_PROJECT_PATH} -logFile"

# Replace Unity Runtime Version
echo "Setting scripting runtime version: ${SCRIPT_RUNTIME_VERSION}"
sed -i -e "s/scriptingRuntimeVersion: 0/scriptingRuntimeVersion: ${SCRIPT_VERSION}/g" ${UNITY_PROJECT_SETTINGS_PATH}/ProjectSettings.asset
cat ${UNITY_PROJECT_SETTINGS_PATH}/ProjectSettings.asset

python3 BuuPackageManager/dependencyResolver.py --rootPackageDirectory ${ROOT_PACKAGE_DIR} ${UNITY_ASSETS_DIR}
python3 BuuPackageManager/dependencyMetaReplacer.py $(pwd) ${ROOT_PACKAGE_DIR} ${UNITY_ASSETS_DIR}
python3 BuuPackageManager/dependencyCleaner.py $(pwd) ${UNITY_ASSETS_DIR}
python3 BuuPackageManager/dependencyIntegrator.py $(pwd) ${UNITY_ASSETS_DIR} ${ROOT_PACKAGE_DIR}
PACKAGE_PATH_LIST=$(cat includedPackagesPathList)
ROOT_PACKAGE_NAME=$(cat rootPackageName)
ROOT_PACKAGE_PATH=$(cat rootPackagePath)
ROOT_PACKAGE_FIRST_NAMESPACE=$(cat rootPackageFirstNamespace)
echo "Root package: ${ROOT_PACKAGE_NAME} and dependencies: ${PACKAGE_PATH_LIST}"
echo "First namespace: ${ROOT_PACKAGE_FIRST_NAMESPACE}"

UNITY_ROOT_PACKAGE_DIR=${UNITY_ASSETS_DIR}/${ROOT_PACKAGE_NAME}
echo "Move rootpackage into Unity project. ${ROOT_PACKAGE_DIR} -> ${UNITY_ROOT_PACKAGE_DIR}"
mv ${ROOT_PACKAGE_DIR}/* ${UNITY_ROOT_PACKAGE_DIR}/
rm ${UNITY_ROOT_PACKAGE_DIR}/buuPackage.yml

${SHARED_CI}/unity3d/execute-unity.sh "$(pwd)/UnityProject" "-batchmode -scripting-runtime-version latest -projectPath $(pwd)/UnityProject -exportPackage Assets/${ROOT_PACKAGE_FIRST_NAMESPACE} $(pwd)/${ROOT_PACKAGE_NAME}.unitypackage -logFile ${CI_PROJECT_DIR}/logs/export-package.log -quit"
