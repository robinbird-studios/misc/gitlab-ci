#!/usr/bin/env bash

UNITY_PROJECT_PATH=$1
UNITY_ARGS=$2


LOCKFILE="~/.locks/gitlab-unity-install.lock"

acquire_lock() {
    local start_time=$(date +%s)

    while [ -e "$LOCKFILE" ]; do
        local current_time=$(date +%s)
        local elapsed_time=$((current_time - start_time))
        echo "Lock file exists. Waiting... (${elapsed_time})"
        sleep 30


        # if [ $elapsed_time -ge $LOCK_TIMEOUT ]; then
        #     echo "Timeout waiting for lock. Exiting."
        #     exit 1
        # fi
    done

    # Create the lock file
    mkdir -p $(dirname "$LOCKFILE")
    touch "$LOCKFILE"
    echo "Lock acquired."
}

release_lock() {
    rm -f "$LOCKFILE"
    echo "Lock released."
}



if [[ -z ${UNITY_INSTALLATIONS} ]]; then
    UNITY_INSTALLATIONS="/Applications/Unity/Hub/Editor"
fi

if [[ -z ${UNITY_EXECUTABLE_RELATIVE_PATH} ]]; then
    UNITY_EXECUTABLE_RELATIVE_PATH="Unity.app/Contents/MacOS/Unity"
fi

if [ ! -z ${UNITY_VERSION+x} ]; then
    echo "Unity version pulled from environment. Installing not supported because we don't know the version details"
    UNITY_PATH=${UNITY_INSTALLATIONS}/${UNITY_VERSION}/${UNITY_EXECUTABLE_RELATIVE_PATH}

    if [ ! -f ${UNITY_PATH} ]; then
        echo "Abort. Could not find Unity version at path: ${UNITY_PATH}.\nEither install it manually or let Unity auto detect and auto install by not specifying UNITY_VERSION variable."
        exit 1
    fi
else
    echo "Auto detecting version based on ProjectSettings.asset"
    UNITY_VERSION=$(cat ${UNITY_PROJECT_PATH}/ProjectSettings/ProjectVersion.txt | shyaml get-value m_EditorVersion)
    UNITY_VERSION_DETAIL=$(cat ${UNITY_PROJECT_PATH}/ProjectSettings/ProjectVersion.txt | shyaml get-value m_EditorVersionWithRevision)
    UNITY_VERSION_CHANGESET=$([[ "${UNITY_VERSION_DETAIL}" =~ \(([0-9a-z]+)\) ]] && echo "${BASH_REMATCH[1]}")
    UNITY_PATH=${UNITY_INSTALLATIONS}/${UNITY_VERSION}/${UNITY_EXECUTABLE_RELATIVE_PATH}

    echo "Detected Unity Version: ${UNITY_VERSION} with detail: ${UNITY_VERSION_DETAIL} changeset: ${UNITY_VERSION_CHANGESET}"

    # TODO: Add platform install detection
    # TODO: Add lock so not multiple installs happen at the same time
    # TODO: Delete install dir when job fails
    acquire_lock
    if [ ! -f ${UNITY_PATH} ]; then
        # Exit with special return code so we know we should install
        echo "Could not find Unity version at path: ${UNITY_PATH}"
        /Applications/Unity\ Hub.app/Contents/MacOS/Unity\ Hub -- --headless install --version ${UNITY_VERSION_DETAIL} --changeset ${UNITY_VERSION_CHANGESET} --module android ios mac-il2cpp webgl --childModules --architecture arm64
    fi
    release_lock
fi

echo "Executing Unity version at: ${UNITY_PATH}"
echo "Using arguments: ${UNITY_ARGS}"

${UNITY_PATH} ${UNITY_ARGS}

UNITY_EXIT_CODE=$?

if [ ${UNITY_EXIT_CODE} -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ ${UNITY_EXIT_CODE} -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ ${UNITY_EXIT_CODE} -eq 3 ]; then
  echo "Run failure (other failure)";
  exit 3
else
  echo "Unexpected exit code ${UNITY_EXIT_CODE}";
  exit ${UNITY_EXIT_CODE}
fi
