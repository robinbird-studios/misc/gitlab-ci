import os
import requests
import argparse


parser = argparse.ArgumentParser(description="Unregister/Delete GitLab runner from all projects")
parser.add_argument("runnerId", help="The Id of the Runner to unregister.")
parser.add_argument("authToken", help="GitLab Auth token to authenticate the requests.")

args = parser.parse_args()

RUNNER_ID = args.runnerId
GITLAB_AUTH_TOKEN = args.authToken

headers = {"PRIVATE-TOKEN": GITLAB_AUTH_TOKEN}

r = requests.get(f"https://gitlab.com/api/v4/runners/{RUNNER_ID}", headers=headers)
runner_data = r.json()

print(f"Got runner data: {runner_data}")

for project in runner_data.get("projects", []):
    print(f"Removing from project: {project['id']}")
    r = requests.delete(
        f"https://gitlab.com/api/v4/projects/{project['id']}/runners/{RUNNER_ID}",
        headers=headers,
    )
    if not r.ok:
        print("Encountered an error deleting runner from project:", r.json())

r = requests.delete(f"https://gitlab.com/api/v4/runners/{RUNNER_ID}", headers=headers)
if not r.ok:
    print("Encountered an error deleting runner:", r.json())