import os
import requests
import argparse


parser = argparse.ArgumentParser(description="Delete amount of pipelines created before a date")
parser.add_argument("projectId", help="The project id.")
parser.add_argument("authToken", help="GitLab Auth token to authenticate the requests.")
parser.add_argument("updatedBefore", help="The date of what pipelines should be deleted. E.g. 2022-06-01. All older pipelines will be deleted.")

args = parser.parse_args()

PROJECT_ID = args.projectId
GITLAB_AUTH_TOKEN = args.authToken
UPDATED_BEFORE = args.updatedBefore

headers = {"PRIVATE-TOKEN": GITLAB_AUTH_TOKEN}

r = requests.get(f"https://gitlab.com/api/v4/projects/{PROJECT_ID}/pipelines/?oder_by=created_at&sort=asc&per_page=100&updated_before={UPDATED_BEFORE}", headers=headers)
runner_data = r.json()

print(f"Got runner data: {runner_data}")

for pipeline in runner_data:
    pipeline_id = pipeline['id']
    print(f"Removing pipeline: {pipeline_id}")
    r = requests.delete(
        f"https://gitlab.com/api/v4/projects/{PROJECT_ID}/pipelines/{pipeline_id}",
        headers=headers,
    )
    if not r.ok:
        print("Encountered an error deleting runner from project:", r.json())
    else:
        print(f"Deteled pipeline {pipeline_id}")


