terraform {
  required_providers {
    sentry = {
      source = "jianyuan/sentry"
      version = "~> 0.9"
    }
  }
}

// Set SENTRY_AUTH_TOKEN for this to work

data "sentry_organization" "main" {
  slug = "robinbird-studios"
}

resource "sentry_team" "main_team" {
  organization = data.sentry_organization.main.id
  name = var.sentry_team_name
}

resource "sentry_project" "backend_project" {
  organization = sentry_team.main_team.organization
  teams         = [sentry_team.main_team.slug]

  name     = "${var.sentry_project_name_prefix}-backend"
  platform = "dotnet-aspnetcore"
}

resource "sentry_project" "unity_browser" {
  organization = sentry_team.main_team.organization
  teams        = [sentry_team.main_team.slug]

  name     = "${var.sentry_project_name_prefix}-browser"
  platform = "unity"
}