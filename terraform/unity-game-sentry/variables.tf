variable "sentry_team_name" {
  description = "A team that houses all projects for this game. Usually the game id like robinbird-brewtycoon"
  type        = string
}

variable "sentry_project_name_prefix" {
  description = "The prefix applied before each project. Usually this would just be the game name like 'brewtycoon' which would result in project names like 'brewtycoon-backend'"
  type        = string
}