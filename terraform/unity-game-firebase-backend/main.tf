# Terraform configuration to set up providers by version.
terraform {
  required_providers {
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.0"
    }
    onepassword = {
      source = "1Password/onepassword"
      version = "2.1.0"
    }
  }
}

# Configure the provider to use a specific project, by default.
# In this guide, this project is referred to as the "quota-check project".
# Including `user_project_override = true` is required if a `billing_project` is specified.
provider "google-beta" {
  billing_project       = var.google_project_terraform_billing_project_id
  user_project_override = true
}


# Create a new Google Cloud project.
resource "google_project" "default" {
  provider = google-beta

  name       = var.google_project_display_name
  project_id = var.google_project_id
  org_id = var.google_project_org_id
  # Required for any service that requires the Blaze pricing plan
  # (like Firebase Authentication with GCIP)
  billing_account = var.google_project_billing_account

  # Required for the project to display in any list of Firebase projects.
  labels = {
    "firebase" = "enabled"
  }
}

# Enable Firebase services for the new project created above.
resource "google_firebase_project" "default" {
  provider = google-beta

  project = google_project.default.project_id
}

# Create a Firebase Android App in the new project created above.
resource "google_firebase_android_app" "default" {
  provider = google-beta

  project      = google_project.default.project_id
  display_name = var.app_display_name
  package_name = var.app_package_name

  # Wait for Firebase to be enabled in the Google Cloud project before creating this App.
  depends_on = [
    google_firebase_project.default,
  ]
}

resource "google_firebase_apple_app" "default" {
  provider = google-beta

  project      = google_project.default.project_id
  display_name = var.app_display_name
  bundle_id    = var.app_package_name
  #app_store_id = 
  team_id = var.apple_team_id

  # Wait for Firebase to be enabled in the Google Cloud project before creating this App.
  depends_on = [
    google_firebase_project.default,
  ]
}

resource "google_firebase_web_app" "default" {
  provider = google-beta

  project      = google_project.default.project_id
  display_name = var.app_display_name

  # Wait for Firebase to be enabled in the Google Cloud project before creating this App.
  depends_on = [
    google_firebase_project.default,
  ]
}

resource "google_project_service" "firestore" {
  provider = google-beta

  project = google_project.default.project_id
  service = "firestore.googleapis.com"

  depends_on = [
    google_firebase_project.default,
  ]
}

resource "google_firestore_database" "database" {
  provider = google-beta

  project                     = google_project.default.project_id
  name                        = "(default)"
  location_id                 = "europe-west1" # Region supported by Cloud Run (https://cloud.google.com/run/docs/locations) and Firestore (https://cloud.google.com/firestore/docs/locations) and carbon friendly
  type                        = "FIRESTORE_NATIVE"
  concurrency_mode            = "OPTIMISTIC"
  app_engine_integration_mode = "DISABLED"

  depends_on = [google_project_service.firestore]
}


## We can't just delete/remove/override the current version. So we create another version and always keep two. This way Terraform does not destory the latest version
# Copy the latest version and make changes
# Remove the oldest version
# Replace the version string in 'google_firebaserules_release' resource
# Run terraform apply
# You are done

# Optional: Use terraform import module.unity-game.google_firebaserules_release.primary projects/robinbird-mineralz/releases/cloud.firestore to import the current state
resource "google_firebaserules_release" "primary" {
  provider     = google-beta
  name         = "cloud.firestore"
  ruleset_name = "projects/${google_project.default.project_id}/rulesets/${google_firebaserules_ruleset.version15.name}"
  project      = google_project.default.project_id
}


resource "google_firebaserules_ruleset" "version14" {
  provider = google-beta
  source {
    files {
      content = <<EOT
rules_version = '2';
service cloud.firestore {
    match /databases/{database}/documents {
        match /users/{userId} {
            // User section where clients can write directly if authenticated
            match /personal/{document=**} {
                allow read, write: if request.auth != null && request.auth.uid == userId;
            }
            // Backend only section per user
            match /server/{document=**} {
      	        allow read, write: if false;
            }
        }
        // Backend only section for all users.
        match /server/{document=**} {
            allow read, write: if false;
        }
    }
}
EOT
      name    = "firestore.rules"
    }
  }
  project = google_project.default.project_id
}

resource "google_firebaserules_ruleset" "version15" {
  provider = google-beta
  source {
    files {
      content = <<EOT
rules_version = '2';
service cloud.firestore {
    match /databases/{database}/documents {
        match /users/{userId} {
            // User section where clients can write directly if authenticated.
            match /personal/{document=**} {
                allow read, write: if request.auth != null && request.auth.uid == userId;
            }
            // Backend only section per user.
            match /server/{document=**} {
      	        allow read, write: if false;
            }
        }
        // Backend only section for all users.
        match /server/{document=**} {
            allow read, write: if false;
        }
    }
}
EOT
      name    = "firestore.rules"
    }
  }
  project = google_project.default.project_id
}

resource "google_project_iam_binding" "owner" {
  project = google_project.default.project_id
  role    = "roles/owner"

  members = [
    "serviceAccount:${var.google_project_sdk_service_account_email}"
  ]
}

resource "google_pubsub_topic" "billing-pubsub" {
  name = "billing-pubsub"
  project = google_project.default.project_id
}

resource "google_billing_budget" "budget" {
  billing_account = var.google_project_billing_account
  display_name    = "${var.app_display_name} Budget Tracking"

  budget_filter {
    projects = ["projects/${var.google_project_id}"]
    credit_types_treatment = "INCLUDE_SPECIFIED_CREDITS"
    credit_types = ["FREE_TIER"]
  }

  amount {
    specified_amount {
      currency_code = "EUR"
      units         = "5"
    }
  }

  threshold_rules {
    threshold_percent = 0.5
  }
  threshold_rules {
    threshold_percent = 1.0
  }
  threshold_rules {
    threshold_percent = 1.0
    spend_basis       = "FORECASTED_SPEND"
  }

  all_updates_rule {
    pubsub_topic = google_pubsub_topic.billing-pubsub.id
  }
}


resource "google_service_account" "backend_runner_service_account" {
  project = google_project.default.project_id
  account_id   = "backend-runner"
  display_name = "${var.app_display_name} Backend Runner"
  description = "Run CloudRun backend containers"
}

resource "google_project_iam_member" "backend_runner_token_creator" {
  project = google_project.default.project_id
  role    = "roles/iam.serviceAccountTokenCreator"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_secret_viewer" {
  project = google_project.default.project_id
  role    = "roles/secretmanager.viewer"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_secret_accessor" {
  project = google_project.default.project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_firebase_service_agent" {
  project = google_project.default.project_id
  role    = "roles/firestore.serviceAgent"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_cloudconfig_viewer" {
  project = google_project.default.project_id
  role    = "roles/cloudconfig.viewer"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_firebase_auth" {
  project = google_project.default.project_id
  role    = "roles/firebaseauth.admin"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}
resource "google_project_iam_member" "backend_runner_firebase_admin_agent" {
  project = google_project.default.project_id
  role    = "roles/firebase.sdkAdminServiceAgent"
  member  = "serviceAccount:${google_service_account.backend_runner_service_account.email}"
}