#!/usr/local/bin/python3
import os

import requests
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

slack_channel_name = os.environ['CI_PROJECT_NAME']

client = WebClient(token=os.environ['SLACK_API_TOKEN'])
channels_json = client.conversations_list()

slack_channel_id = ""
for channel in channels_json["channels"]:
	if channel["name"] == slack_channel_name:
		slack_channel_id = channel["id"]
		break

if slack_channel_id == "":
	print(f"Not sending to slack channel id could not be retrieved for name {slack_channel_name}")
	exit(0)

try:
	project_name = os.environ['CI_PROJECT_NAME']
	pipeline_url = os.environ['CI_PIPELINE_URL']
	commit_ref_name = os.environ['CI_COMMIT_REF_NAME'] #branch/tag/sha
	commit_description = os.environ['CI_COMMIT_DESCRIPTION'].strip()
	
	client.chat_postMessage(channel=slack_channel_id, text="Build Failed", blocks=
	[
		{
			"type": "header",
			"text": {
				"type": "plain_text",
				"text": f"{project_name} build FAILED :fire:"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"Ref: {commit_ref_name}\n Description: {commit_description}"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"{project_name} reported a failed job <{pipeline_url}|Here>"
			}
		}
	])
except SlackApiError as e:
	# You will get a SlackApiError if "ok" is False
	assert e.response["ok"] is False
	assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
	print(f"Got an error: {e.response['error']}")
	exit(1)
