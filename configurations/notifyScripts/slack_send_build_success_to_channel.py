#!/usr/local/bin/python3
import os

import requests
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

slack_channel_name = os.environ['CI_PROJECT_NAME']

client = WebClient(token=os.environ['SLACK_API_TOKEN'])
channels_json = client.conversations_list()

slack_channel_id = ""
for channel in channels_json["channels"]:
	if channel["name"] == slack_channel_name:
		slack_channel_id = channel["id"]
		break

if slack_channel_id == "":
	print(f"Not sending to slack channel id could not be retrieved for name {slack_channel_name}")
	exit(0)

try:
	project_name = os.environ['CI_PROJECT_NAME']
	pipeline_url = os.environ['CI_PIPELINE_URL']
	commit_ref_name = os.environ['CI_COMMIT_REF_NAME'] #branch/tag/sha
	commit_description = os.environ['CI_COMMIT_DESCRIPTION'].strip()

	blocks = [
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"*{project_name.capitalize()}* #{commit_ref_name} build finished with success! :white_check_mark:"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"Ref: {commit_ref_name}\nDescription: {commit_description}\n<{pipeline_url}|Open GitLab Pipeline>"
			}
		}
	]

	try:
		ios_url = os.environ['FIREBASE_DISTRO_IOS_TESTER_LINK']
	except KeyError:
		ios_url = None

	if ios_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{ios_url}|:green_apple: iOS {os.environ['FIREBASE_DISTRO_IOS_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_IOS_BUILD_VERSION']})>"
			}
		})

	try:
		android_url = os.environ['FIREBASE_DISTRO_ANDROID_TESTER_LINK']
	except KeyError:
		android_url = None

	if android_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{android_url}|:robot_face: Android  {os.environ['FIREBASE_DISTRO_ANDROID_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_ANDROID_BUILD_VERSION']})>"
			}
		})

	try:
		web_url = os.environ['FIREBASE_DISTRO_WEB_TESTER_LINK']
	except KeyError:
		web_url = None

	if web_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{web_url}|:globe_with_meridians: Web {os.environ['FIREBASE_DISTRO_WEB_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_WEB_BUILD_VERSION']})>"
			}
		})

	#TODO: Add Admin tool?
	#TODO: Add Backend tool?

	client.chat_postMessage(
		channel=slack_channel_id,
		text="Build Success",
		blocks=blocks
	)

except SlackApiError as e:
	# You will get a SlackApiError if "ok" is False
	assert e.response["ok"] is False
	assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
	print(f"Got an error: {e.response['error']}")
	exit(1)
