#!/bin/bash

TEST_RESULT_STRING=$(cat $1)
pat='failures=\"([1-9][0-9]*)\"' # only matches number higher than zero
[[ "${TEST_RESULT_STRING}" =~ $pat ]]
echo "Found failures: ${BASH_REMATCH[1]}"
if [[ "${BASH_REMATCH[0]}" != "" ]]; then echo "Returning error exit code because tests have failures."; exit 1; fi